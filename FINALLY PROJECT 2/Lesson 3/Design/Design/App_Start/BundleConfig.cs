﻿using System.Web;
using System.Web.Optimization;

namespace Design
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));



            bundles.Add(new StyleBundle("~/Content/Home/css").Include(
                "~/Content/Home/Index.css",
                "~/Content/Post/Index.css"));

            bundles.Add(new ScriptBundle("~/Script/Home/Posts").Include(
                "~/Scripts/Home/AddPost.js",
                "~/Scripts/Home/DeletePost.js",
                "~/Scripts/Home/EditPost.js"
                ));

            bundles.Add(new ScriptBundle("~/Script/Home/Friends").Include(
                "~/Scripts/Home/AddFriend.js",
                "~/Scripts/Home/DeleteFriend.js",
                "~/Scripts/Home/DeleteRequest.js",
                "~/Scripts/Home/RefuceRequest.js",
                "~/Scripts/Home/AcceptRequest.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/angular-components").Include(
                "~/Scripts/angular.js",
                "~/Scripts/angular-animate.js",
                "~/Scripts/angular-messages.js",
                "~/Scripts/angular-ui-router.js",
                "~/Scripts/ng-input.js",
                "~/Scripts/ui-bootstrap-custom-2.2.0.min.js",
                "~/Scripts/ui-bootstrap-custom-tpls-2.2.0.min.js",
                "~/Scripts/angular-toastr.tpls.js",
                "~/Scripts/ng-croppie.js",
                "~/Scripts/ng-file-upload-all.min.js",
                "~/Scripts/ng-file-upload.min.js",
                "~/Scripts/freewall.js",
                "~/Scripts/angulargrid.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                      "~/Scripts/Angular/app.js",
                      "~/Scripts/Angular/Home/home.js",
                      "~/Scripts/Angular/Home/homeComponent.js",
                      "~/Scripts/Angular/Home/homeDirective.js",
                      "~/Scripts/Angular/Home/homeService.js",
                      "~/Scripts/Angular/Home/homeFilter.js",
                      "~/Scripts/Angular/Home/homeInputdir.js"
                      ));

        }
    }
}
