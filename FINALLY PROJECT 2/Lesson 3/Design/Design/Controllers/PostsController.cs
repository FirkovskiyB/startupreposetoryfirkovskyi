﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Design.Models;
using Microsoft.AspNet.Identity;

namespace Design.Controllers
{
    [Authorize]
    public class PostsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [HttpGet]
        public ActionResult Index()
        {
            string id = User.Identity.GetUserId();
            return View(db.Posts.Include(c => c.Author).Where(x => x.Author.Id == id).OrderByDescending(x => x.Date).ToList());
        }

        
        public JsonResult AllPosts()
        {
            var posts = db.Posts.Include(c => c.Author).ToList();
            return new JsonResult { Data = posts, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }



        [HttpPost]
        public ActionResult AddPost(Post post)
        {
            string id = User.Identity.GetUserId();
            ApplicationUser user = db.Users.FirstOrDefault(x => x.Id == id);
            post.Author = user;
            post.Date = DateTime.Now;
            db.Posts.Add(post);
            db.SaveChanges();

            return PartialView("PostPartialView", post);
        }

        [HttpPost]
        public ActionResult DeletePost(int id)
        {
            db.Posts.Remove(db.Posts.FirstOrDefault(e => e.Id == id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult EditPost(Post post)
        {
            Post t = db.Posts.Single<Post>(x => x.Id == post.Id);
            t.Title = post.Title;
            t.Text = post.Text;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
