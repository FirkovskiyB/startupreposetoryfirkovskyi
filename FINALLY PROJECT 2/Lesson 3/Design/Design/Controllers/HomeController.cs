﻿using Design.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;


namespace Design.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            string idCurrentUser = User.Identity.GetUserId();
            var userIdParameter = new SqlParameter("@currentId", idCurrentUser);
            var result = db.Database.SqlQuery<UserFriend>("UsersWithRealetion @currentId", userIdParameter).ToList();
            return View(result);
        }

        [HttpPost]
        public ActionResult AddFriend(UserFriend friend)
        {
            
            string idCurrentUser = User.Identity.GetUserId();
            var newFriend = new Friend();

            var findUser = db.Friends.FirstOrDefault(x => (x.FirstFriend.Id == idCurrentUser && x.SecondFriend.Id == friend.Id) ||
                                                          (x.SecondFriend.Id == idCurrentUser && x.FirstFriend.Id == friend.Id));

            if (findUser == null)
            {
                var FirstFriend = db.Users.FirstOrDefault(x => x.Id == idCurrentUser);
                var SecondFriend = db.Users.FirstOrDefault(x => x.Id == friend.Id);

                newFriend.FirstFriend = FirstFriend;
                newFriend.SecondFriend = SecondFriend;
                newFriend.Status = Status.follower;
                db.Friends.Add(newFriend);
                db.SaveChanges();
            } else
            {
                findUser.Status = Status.follower;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DeleteFriend(UserFriend friend)
        {
            string idCurrentUser = User.Identity.GetUserId();
            var findFriend = db.Friends.Remove(db.Friends.FirstOrDefault(x => (x.FirstFriend.Id == idCurrentUser && x.SecondFriend.Id == friend.Id) ||
                                                          (x.SecondFriend.Id == idCurrentUser && x.FirstFriend.Id == friend.Id)));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DeleteRequest(UserFriend friend)
        {
            string idCurrentUser = User.Identity.GetUserId();
            var findFriend = db.Friends.FirstOrDefault(x => x.FirstFriend.Id == idCurrentUser && x.SecondFriend.Id == friend.Id);
            findFriend.Status = Status.empty;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult AcceptRequest(UserFriend friend)
        {
            string idCurrentUser = User.Identity.GetUserId();
            var findFriend = db.Friends.FirstOrDefault(x => x.FirstFriend.Id == friend.Id && x.SecondFriend.Id == idCurrentUser && x.Status == Status.follower);
            findFriend.Status = Status.friend;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult RefuseRequest(UserFriend friend)
        {
            string idCurrentUser = User.Identity.GetUserId();
            var findFriend = db.Friends.Remove(db.Friends.FirstOrDefault(x => x.FirstFriend.Id == friend.Id && x.SecondFriend.Id == idCurrentUser && x.Status == Status.follower));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}