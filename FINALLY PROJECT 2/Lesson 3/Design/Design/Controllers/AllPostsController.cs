﻿using Design.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using System.Data.SqlClient;

namespace Design.Controllers
{
    [Authorize]
    public class AllPostsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: AllPosts
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult getPosts(int lastId = Int32.MaxValue)
        {
            string idCurrentUser = User.Identity.GetUserId();
            var userIdParameter = new SqlParameter("@currentId", idCurrentUser);
            var lastPostIdParameters = new SqlParameter("@lastIdPost", lastId);
            var showNumbersRows = new SqlParameter("@numberRowsToReturns", 5);
            var result = db.Database.SqlQuery<PostWithLike>("LikesOnPosts @currentId, @lastIdPost, @numberRowsToReturns", userIdParameter, lastPostIdParameters, showNumbersRows).ToList();
           
            return new JsonResult { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        //[HttpPost]
        public ActionResult deletePost(int id)
        {
            string idCurrentUser = User.Identity.GetUserId();
            var query = db.Posts.Include(p => p.Author).FirstOrDefault(e => e.Id == id);
            if (query.Author.Id == idCurrentUser)
            {
                db.Posts.Remove(query);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else {
                return Json(new { Success = false});

            }


        }


        public ActionResult EditPost(Post post)
        {
            var t = db.Posts.Single<Post>(x => x.Id == post.Id);
            t.Title = post.Title;
            t.Text = post.Text;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult AddLike (int id)
        {
            string idCurrentUser = User.Identity.GetUserId();                 
            string userId = idCurrentUser;

            Like like = new Like();
            like.PostId = id;
            like.UserId = userId;
            db.Likes.Add(like);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult DeleteLike (int id)
        {
            string idCurrentUser = User.Identity.GetUserId();
            db.Likes.Remove(db.Likes.First(x => x.PostId == id && x.UserId == idCurrentUser));
            db.SaveChanges();
            return RedirectToAction("Index");
        }



    }
}