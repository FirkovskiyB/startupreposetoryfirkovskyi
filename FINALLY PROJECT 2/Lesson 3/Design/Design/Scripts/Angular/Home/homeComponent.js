﻿(function () {
    'use strict';

    function homeController(homeService) {
        var vm = this;
        vm.Posts = homeService;
                
        vm.getResults = function () {
            homeService.gethomeResults().success(function (data) {
                vm.Posts.setPost(data);
            });
        }

        vm.Delete = function (id) {
            console.log("Ctrl"+ id);
            homeService.deletePost(id)
                .success(function () {
                    angular.element("#" + id).remove();
                })
                .error(function (){
                    console.log("ERORR");
                    alert("Ви не можете видаляти чужі пости");
                });
        }

        vm.Show = function (post) {
            homeService.showPopUp = true;
            homeService.Post = post;
            homeService.TempPost = post;
            console.log(homeService.showPopUp);
        }
        vm.Close = function () {
            console.log("byaa");
            homeService.showPopUp = false;
            homeService.Post = homeService.PostTemp;

            //console.log(homeService.PostTemp); 
            //for (item in homeService.Posts)
            //{
            //    if (homeService.Posts[item].id == homeService.PostTemp.id)
            //    { homeService.Posts[item] = homeService.PostTemp;}
            //}
            console.log(homeService.showPopUp);
        }

        vm.EditPost = function (post) {

            homeService.Post = post;
            homeService.editPost(post).success(function (data) {
                homeService.showPopUp = false;
            });
        }

        vm.AddLikeToPost = function (post) {
            homeService.addLike(post.id).success(function () {
                console.log("Controller AddLikePosts success");
                post.CountLike++;
                post.OwnLike = !post.OwnLike;
                console.log(post.OwnLike);
            })
            console.log('hello like2');
        }

        vm.DeleteLikeFromPost = function (post) {
            homeService.deleteLike(post.id).success(function () {
                console.log("Controller DeleteLikeFromPosts success");
                post.CountLike--;
                post.OwnLike = !post.OwnLike;
                console.log(post.OwnLike);
            })
            console.log('hello like2');
        }

        vm.getResults();
    }

    homeController.$inject = ['homeService'];

    angular
        .module('app.home', [])
        .component('homeComponent', {
            templateUrl: '/Scripts/Angular/Home/homeTest.html',
            controller: homeController,
            bindings: {

            }
        });
})()