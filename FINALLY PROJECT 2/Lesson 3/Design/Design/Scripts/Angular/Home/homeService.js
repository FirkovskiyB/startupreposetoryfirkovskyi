﻿(function () {
    'use strict';

    function homeService($http) {
        var factory = {};
        //Отримуємо список постів
        factory.Posts = [];
        factory.Post;


        factory.showPopUp = false;

        factory.gethomeResults = function (lastId) {
            return $http.get("/AllPosts/getPosts", { params: {lastId: lastId}});
        }
        factory.setPost = function (posts) {
            return factory.Posts.push.apply (factory.Posts,posts);
        }
        factory.getPost = function () {
            return factory.Posts;
        }

        factory.deletePost = function(id)
        {
            return $http.get("/AllPosts/deletePost", { params: { id: id } });
        }

        factory.editPost = function (post1) {
            console.log(post1);
            return $http.post("/AllPosts/EditPost",  {post : post1})
        }

        factory.addLike = function (id)
        {
            console.log(id);
            return $http.get("/AllPosts/AddLike", { params: { id: id } })
        }
        factory.deleteLike = function (id) {
            console.log(id);
            return $http.get("/AllPosts/DeleteLike", { params: { id: id } })
        }
        return factory;
    };

    homeService.$inject = ['$http'];

    angular
      .module('home.service', [])
      .factory('homeService', homeService);

})();