﻿(function () {
    'use strict';

    function scrollBottom(homeService) {
        
        return {
            restrict: 'AE',
            scope: true,
            link: function (element, attrs) {
                window.onscroll = function () {
                        if (document.body.scrollTop == document.body.offsetHeight - window.innerHeight ) {
                            var id = $(".post").last().attr("id");
                            console.log(id);
                            homeService.gethomeResults(id).success(function (data) {
                                homeService.setPost(data);
                                console.log(data);
                            });
                            
                        }
                    
                };
            }
        };
    };


    scrollBottom.$inject = ['homeService'];

    angular
        .module('home.directive', [])
        .directive('scrollBottom', scrollBottom);

})();