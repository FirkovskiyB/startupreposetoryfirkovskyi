﻿(function () {
    angular.module('app.home', [])
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider.state('AllPosts', {
                url: "/AllPosts",
                template: '<home-component></home-component>',
                data: {
                    pageTitle: 'AllPosts',
                }
            });

        }]);
})();