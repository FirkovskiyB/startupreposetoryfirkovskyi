﻿(function () {
    'use strict';

    function inputMenu(homeService) {
        return {
            restrict: 'E',
            scope: {
                show: '='
            },
            templateUrl: '/Scripts/Angular/Home/inputMenu.html',
            controller: function ($scope) {
               
            }
        }
    }

    inputMenu.$inject = ['homeService'];

    angular
        .module('home.inputDir', [])
        .directive('inputMenu', inputMenu);

})();