﻿$(function () {
    $('body').on('click', '.addFriend', function (event) {
        var id1 = $(this).attr("id");
        
        idPost = id1.replace('addFriend_', '');

        var dataObj = {
            id: idPost
        }

        $.ajax({
            type: "POST",
            url: "/Home/AddFriend",
            data: dataObj,
            success: function () {
                var newButton = '<div class="col-md-4 col-sm-5 col-xs-4 right-block " id="right-block_' + idPost + '"> <button type="button" class="btn btn-info deleteRequest" id="deleteRequest_' + idPost + '">Delete request</button> </div>';
                $("#right-block_" + idPost).replaceWith(newButton);
            }

        });
        });
    });