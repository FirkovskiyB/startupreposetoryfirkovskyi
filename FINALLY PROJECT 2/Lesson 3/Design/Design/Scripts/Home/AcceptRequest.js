﻿$(function () {
    $('body').on('click', '.addNewFriendRequest', function (event) {
        var id1 = $(this).attr("id");

        idPost = id1.replace('addNewFriendRequest_', '');

        var dataObj = {
            Id: idPost
        }

        $.ajax({
            type: "POST",
            url: "/Home/AcceptRequest",
            data: dataObj,
            success: function () {
                var newButton = '<div class="col-md-4 col-sm-5 col-xs-4 right-block " id="right-block_' + idPost + '"> <button type="button" class="btn btn-primary deleteFriend" id="deleteFriend_' + idPost + '">Delete Friend</button> </div>';
                $("#right-block_" + idPost).replaceWith(newButton);
            }

        });
    });
});