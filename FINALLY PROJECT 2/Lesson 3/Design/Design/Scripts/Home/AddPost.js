﻿$(function () {
    $(".btn-send").click(function () {
        var dataObj = {
            Title: $("#Tit").val(),
            Text: $("#Tex").val()
        }

        $.ajax({
            type: "POST",
            url: "/Posts/AddPost",
            data: dataObj,
            success: function (data) {
                $("#showInformationUser").prepend(data);
            }

        });
    });

})