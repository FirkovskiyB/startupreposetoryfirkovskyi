﻿$(function () {
    $('body').on('click', '.deleteNewFriendRequest', function (event) {
        var id1 = $(this).attr("id");

        idPost = id1.replace('deleteNewFriendRequest_', '');

        var dataObj = {
            Id: idPost
        }

        $.ajax({
            type: "POST",
            url: "/Home/RefuseRequest",
            data: dataObj,
            success: function () {
                var newButton = '<div class="col-md-4 col-sm-5 col-xs-4 right-block" id="right-block_' + idPost + '"> <button type="button" class="btn btn-default addFriend" id="addFriend_' + idPost + '">AddFriend</button> </div>';

                $("#right-block_" + idPost).replaceWith(newButton);
            }

        });
    });
});