namespace Design.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class someUpdatesToLike : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Likes", "PostId", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Likes", "PostId", c => c.Int(nullable: false));
        }
    }
}
