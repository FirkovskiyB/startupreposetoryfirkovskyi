namespace Design.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewTableFriends : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Friends",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Status = c.Int(nullable: false),
                        FirstFriend_Id = c.String(maxLength: 128),
                        SecondFriend_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.FirstFriend_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.SecondFriend_Id)
                .Index(t => t.FirstFriend_Id)
                .Index(t => t.SecondFriend_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Friends", "SecondFriend_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Friends", "FirstFriend_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Friends", new[] { "SecondFriend_Id" });
            DropIndex("dbo.Friends", new[] { "FirstFriend_Id" });
            DropTable("dbo.Friends");
        }
    }
}
