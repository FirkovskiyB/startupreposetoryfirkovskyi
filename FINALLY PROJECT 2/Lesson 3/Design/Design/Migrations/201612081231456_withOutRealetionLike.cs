namespace Design.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class withOutRealetionLike : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Likes", "Post_Id", "dbo.Posts");
            DropForeignKey("dbo.Likes", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Likes", new[] { "Post_Id" });
            DropIndex("dbo.Likes", new[] { "User_Id" });
            AddColumn("dbo.Likes", "PostId", c => c.Int(nullable: false));
            AddColumn("dbo.Likes", "UserId", c => c.String());
            DropColumn("dbo.Likes", "Post_Id");
            DropColumn("dbo.Likes", "User_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Likes", "User_Id", c => c.String(maxLength: 128));
            AddColumn("dbo.Likes", "Post_Id", c => c.Int());
            DropColumn("dbo.Likes", "UserId");
            DropColumn("dbo.Likes", "PostId");
            CreateIndex("dbo.Likes", "User_Id");
            CreateIndex("dbo.Likes", "Post_Id");
            AddForeignKey("dbo.Likes", "User_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Likes", "Post_Id", "dbo.Posts", "Id");
        }
    }
}
