namespace Design.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangePostModel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PostModels", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.PostModels", new[] { "UserId" });
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Text = c.String(),
                        Date = c.DateTime(nullable: false),
                        Author_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.Author_Id)
                .Index(t => t.Author_Id);
            
            DropTable("dbo.PostModels");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PostModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Text = c.String(),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.Posts", "Author_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Posts", new[] { "Author_Id" });
            DropTable("dbo.Posts");
            CreateIndex("dbo.PostModels", "UserId");
            AddForeignKey("dbo.PostModels", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
    }
}
