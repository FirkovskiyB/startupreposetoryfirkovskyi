﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Design.Models
{
    public enum Status
    {
        friend = 0,
        follower = 1,
        empty = 2
    }
    public class Friend
    {
        public int Id { get; set; }
        public ApplicationUser FirstFriend { get; set; }
        public ApplicationUser SecondFriend { get; set; }
        public Status Status { get; set; }

    }
}