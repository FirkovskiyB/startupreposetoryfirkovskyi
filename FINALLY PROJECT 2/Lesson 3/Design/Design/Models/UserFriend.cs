﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Design.Models
{
    public class UserFriend
    {

        public string Id         { get; set; }
        public string LastName   { get; set; }
        public string FirstName  { get; set; }
        public string Email      { get; set; }
        public int Status        { get; set; }
        public int IdInFriendsDb { get; set; }
        public bool Follower      { get; set; }
    }
}