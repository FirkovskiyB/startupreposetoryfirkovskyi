﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Design.Models
{
    public class PostWithLike
    {
        public int id { get; set; }
        public string AuthorPostEmail { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
        public int CountLike { get; set; }
        public bool OwnLike  { get; set; }
        public bool OwnPost  { get; set; }
    }
}