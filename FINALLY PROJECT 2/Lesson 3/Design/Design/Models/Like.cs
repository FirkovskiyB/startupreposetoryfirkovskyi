﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Design.Models
{
    public class Like
    {
        public int Id { get; set; }
        public int? PostId { get; set; }
        public string UserId { get; set; }
    }
}