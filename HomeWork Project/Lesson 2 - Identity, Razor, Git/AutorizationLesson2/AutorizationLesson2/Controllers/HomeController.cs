﻿using AutorizationLesson2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace AutorizationLesson2.Controllers
{
    
    [Authorize]
    public class HomeController : Controller
    {

        ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {

            // var listPosts = db.Users.First(x => x.Id == User.Identity.GetUserId().ToString());
            var listPosts = db.Users.Single(u => u.Email == User.Identity.Name);
            //var listPosts = db.Users.ToList();
            return View(listPosts);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            return Redirect("/home/index");
        }
    }
}