﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AutorizationLesson2.Startup))]
namespace AutorizationLesson2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
