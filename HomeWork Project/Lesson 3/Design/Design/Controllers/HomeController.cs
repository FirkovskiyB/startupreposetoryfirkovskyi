﻿using Design.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;


namespace Design.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public ActionResult Post()
        {
            return View(db.Posts.ToList());
        }

        [HttpPost]
        public ActionResult AddPost(Post post)
        {
            string id = User.Identity.GetUserId();
            ApplicationUser user = db.Users.FirstOrDefault(x => x.Id==id);
            post.Author = user;
            post.Date = DateTime.Now;
            db.Posts.Add(post);
            db.SaveChanges();

            return PartialView("PostPartialView", post);
        }

        [HttpPost]
        public ActionResult DeletePost(int id)
        {
            db.Posts.Remove(db.Posts.FirstOrDefault(e => e.Id == id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult EditPost(Post post)
        {
            Post t = db.Posts.Single<Post>(x => x.Id == post.Id);
            t.Title = post.Title;
            t.Text = post.Text;
            db.SaveChanges();
            return RedirectToAction("Index");
            //if (ModelState.IsValid)
            //{
            //    db.Entry(post).State = EntityState.Modified;
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}
            //return View(post);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}