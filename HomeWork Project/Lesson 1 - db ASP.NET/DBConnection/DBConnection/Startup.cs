﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DBConnection.Startup))]
namespace DBConnection
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
