namespace DBConnection_v._1._1.DAL
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class PostContex : DbContext
    {

        public PostContex()
            : base("name=PostContex")
        {
        }
        public virtual DbSet<Post> Posts { get; set; }
    }


}