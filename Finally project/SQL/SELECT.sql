USE StartUpSoft_ex2
GO

----------SELECTS--------------
SELECT * FROM Users
SELECT * FROM Friends

----------SHOW FRIEND USER--------------
SELECT fr.FirstFriend, fr.SecondFriend, u1.FirstName as 'User first Name',u1.LastName as 'User last name', u2.FirstName as 'Friend First Name',u2.LastName as 'Friend Last Name', fr.[Status]
FROM Friends fr
INNER JOIN Users u1
ON fr.FirstFriend = u1.IdUser
INNER JOIN Users u2
ON fr.SecondFriend = u2.IdUser
WHERE (fr.FirstFriend = 1 or fr.SecondFriend =1) and fr.[Status] like 'friend'
----------SHOW FOLOWER USER--------------
SELECT fr.FirstFriend, fr.SecondFriend, u1.FirstName as 'User first Name',u1.LastName as 'User last name', u2.FirstName as 'Folower First Name',u2.LastName as 'Follower Last Name', fr.[Status]
FROM Friends fr
INNER JOIN Users u1
ON fr.FirstFriend = u1.IdUser
INNER JOIN Users u2
ON fr.SecondFriend = u2.IdUser
WHERE fr.FirstFriend = 2 and fr.[Status] like 'follower'
----------SHOW SIGNED USER--------------
SELECT fr.FirstFriend, fr.SecondFriend, u1.FirstName as 'Signed user first Name',u1.LastName as 'Signed user last name', u2.FirstName as 'USer First Name',u2.LastName as 'User Last Name', fr.[Status]
FROM Friends fr
INNER JOIN Users u1
ON fr.FirstFriend = u1.IdUser
INNER JOIN Users u2
ON fr.SecondFriend = u2.IdUser
WHERE fr.SecondFriend = 3 and fr.[Status] like 'follower'
