USE StartUpSoft_ex2
GO

----------INSERT INFO--------------
INSERT INTO Users 
VALUES 
('Firkovskiy','Bohdan','bohdan@gmail.com','2105'),
('Krychinin','Andriy','andriy@gmail.com','2101'),
('Khomyn','Mihaylo','mihaylo@gmail.com','1205'),
('USER_LAST','USER_FIRST','user@gmail.com','1205')
GO

INSERT INTO Friends
VALUES
(1,2,'friend'),
(1,3,'friend'),
(2,3,'follower'),
(4,1,'friend')
GO