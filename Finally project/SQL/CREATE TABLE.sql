USE StartUpSoft_ex2
GO

CREATE TABLE Users 
(
	IdUser int identity(1,1) NOT NULL,
	LastName varchar(255),
	FirstName varchar(255),
	Email varchar(255) NOT NULL,
	[Password] varchar(255) NOT NULL,
	CONSTRAINT pk_IdUser PRIMARY KEY CLUSTERED ([IdUser] ASC),
	CONSTRAINT corectly_email CHECK (Email like '%_@_%_.__%')
)
GO
CREATE TABLE Friends
(
	Id int identity(1,1) NOT NULL,
	FirstFriend int  NOT NULL,
	SecondFriend int NOT NULL,
	[Status] varchar(10) NOT NULL,
	CONSTRAINT PK_idFriends PRIMARY KEY CLUSTERED([Id] ASC),

	CONSTRAINT FK_FirstFriend FOREIGN KEY(FirstFriend) 
	REFERENCES Users(IdUser),
	CONSTRAINT FK_SecondFriend FOREIGN KEY(SecondFriend) 
	REFERENCES Users(IdUser),
	CONSTRAINT CH_Status CHECK(
	[Status] in ('friend','follower'))
)
GO