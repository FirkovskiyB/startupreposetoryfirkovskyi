USE StartUpSoft_ex2
GO

----------DROP TABLE--------------
DROP TABLE Friends
DROP TABLE Users
GO

----------CREATE TABLE--------------
CREATE TABLE Users 
(
	IdUser int identity(1,1) NOT NULL,
	LastName varchar(255),
	FirstName varchar(255),
	Email varchar(255) NOT NULL,
	[Password] varchar(255) NOT NULL,
	CONSTRAINT pk_IdUser PRIMARY KEY ([IdUser]),
	CONSTRAINT corectly_email CHECK (Email like '%_@_%_.__%')
)
GO
CREATE TABLE Friends
(
	Id int identity(1,1) NOT NULL,
	FirstFriend int  NOT NULL,
	SecondFriend int NOT NULL,
	[Status] varchar(10) NOT NULL,
	CONSTRAINT PK_idFriends PRIMARY KEY ([Id]),

	CONSTRAINT FK_FirstFriend FOREIGN KEY(FirstFriend) 
	REFERENCES Users(IdUser),
	CONSTRAINT FK_SecondFriend FOREIGN KEY(SecondFriend) 
	REFERENCES Users(IdUser),
	CONSTRAINT CH_Status CHECK(
	[Status] in ('friend','follower'))
)
GO

----------INSERT INFO--------------
INSERT INTO Users 
VALUES 
('Firkovskiy','Bohdan','bohdan@gmail.com','2105'),
('Krychinin','Andriy','andriy@gmail.com','2101'),
('Khomyn','Mihaylo','mihaylo@gmail.com','1205'),
('USER_LAST','USER_FIRST','user@gmail.com','1205')
GO

INSERT INTO Friends
VALUES
(1,2,'friend'),
(1,3,'friend'),
(2,3,'follower'),
(4,1,'friend')
GO

----------SELECTS--------------
SELECT * FROM Users
SELECT * FROM Friends

----------SHOW FRIEND USER--------------
SELECT fr.FirstFriend, fr.SecondFriend, u1.FirstName as 'User first Name',u1.LastName as 'User last name', u2.FirstName as 'Friend First Name',u2.LastName as 'Friend Last Name', fr.[Status]
FROM Friends fr
INNER JOIN Users u1
ON fr.FirstFriend = u1.IdUser
INNER JOIN Users u2
ON fr.SecondFriend = u2.IdUser
WHERE (fr.FirstFriend = 1 or fr.SecondFriend =1) and fr.[Status] like 'friend'
----------SHOW FOLOWER USER--------------
SELECT fr.FirstFriend, fr.SecondFriend, u1.FirstName as 'User first Name',u1.LastName as 'User last name', u2.FirstName as 'Folower First Name',u2.LastName as 'Follower Last Name', fr.[Status]
FROM Friends fr
INNER JOIN Users u1
ON fr.FirstFriend = u1.IdUser
INNER JOIN Users u2
ON fr.SecondFriend = u2.IdUser
WHERE fr.FirstFriend = 2 and fr.[Status] like 'follower'
----------SHOW SIGNED USER--------------
SELECT fr.FirstFriend, fr.SecondFriend, u1.FirstName as 'Signed user first Name',u1.LastName as 'Signed user last name', u2.FirstName as 'USer First Name',u2.LastName as 'User Last Name', fr.[Status]
FROM Friends fr
INNER JOIN Users u1
ON fr.FirstFriend = u1.IdUser
INNER JOIN Users u2
ON fr.SecondFriend = u2.IdUser
WHERE fr.SecondFriend = 3 and fr.[Status] like 'follower'

----------UPDATE---------
UPDATE Friends 
SET [Status] = 'friend'
WHERE FirstFriend=2 and SecondFriend = 3 and [Status] like 'follower'

----------PROCEDURE------------
CREATE PROC showFriends
	@currentId int   -- ��� ������������� ��������� �������� �� ��������� �� ������.
AS
	SELECT fr.FirstFriend, fr.SecondFriend, u1.FirstName as 'User first Name',u1.LastName as 'User last name', u2.FirstName as 'Friend First Name',u2.LastName as 'Friend Last Name', fr.[Status]
	FROM Friends fr
	INNER JOIN Users u1
	ON fr.FirstFriend = u1.IdUser
	INNER JOIN Users u2
	ON fr.SecondFriend = u2.IdUser
	WHERE (fr.FirstFriend = @currentId or fr.SecondFriend = @currentId)  and fr.[Status] like 'friend'
GO

EXEC showFriends 1



