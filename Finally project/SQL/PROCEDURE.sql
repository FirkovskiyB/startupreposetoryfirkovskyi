USE StartUpSoft_ex2
GO

CREATE PROC showFriends
	@currentId int   -- ��� ������������� ��������� �������� �� ��������� �� ������.
AS
	SELECT fr.FirstFriend, fr.SecondFriend, u1.FirstName as 'User first Name',u1.LastName as 'User last name', u2.FirstName as 'Friend First Name',u2.LastName as 'Friend Last Name', fr.[Status]
	FROM Friends fr
	INNER JOIN Users u1
	ON fr.FirstFriend = u1.IdUser
	INNER JOIN Users u2
	ON fr.SecondFriend = u2.IdUser
	WHERE fr.FirstFriend = @currentId and fr.[Status] like 'friend'
GO