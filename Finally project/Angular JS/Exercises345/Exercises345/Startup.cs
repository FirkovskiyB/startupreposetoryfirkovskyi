﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Exercises345.Startup))]
namespace Exercises345
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
