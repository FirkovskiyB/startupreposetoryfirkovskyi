﻿var myApp = angular.module('App', []);

myApp.directive('firstDir', function () {
    return {
        restrict: 'E',
        templateUrl: 'exampleView.cshtml'
    };
});
myApp.directive('secondDir', function () {
    return {
        restrict: 'E',
        templateUrl: 'CustomerView.cshtml'
    };
});
myApp.controller('myControler', function ($scope) {
    $scope.universityName = "TNEU";
})