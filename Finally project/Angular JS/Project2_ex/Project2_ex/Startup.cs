﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Project2_ex.Startup))]
namespace Project2_ex
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
