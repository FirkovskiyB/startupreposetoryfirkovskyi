﻿var app = angular.module("app", []);

//app.controller('phoneController', function ($scope) {
//    $scope.phone = {
//        name: 'Nokia Lumia 630',
//        year: 2014,
//        price: 200,
//        company: {
//            name: 'Nokia',
//            country: 'Финляндия'
//        }
//    }
//})
app.service('UserService', function ($http) {
    this.getUsers = function () {
        return $http.get('/Home/ShowUsers')
    }
})

app.controller('phoneController', function ($scope, UserService) {
    function getUserList() {
        UserService.getUsers().then(function (emp) {
            $scope.userList = emp.data;
        }, function (error) {
            alert('Failed to fetch data');
        })
    }
})



//app.controller('UserCtrl',
//    function ($scope, UserService) {
//        function getUserList() {
//            UserService.getUsers().then(function (emp) {
//                $scope.userList = emp.data;
//            }, function (error) {
//                alert('Failed to fetch data');
//            })
//        }
//    })
