﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ThirdProject.Startup))]
namespace ThirdProject
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
